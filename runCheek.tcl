
package require FullMonte
#get the location of the script file and cd to
#its parent directory
set script_path [ file dirname [ file normalize [ info script ] ] ]
cd $script_path

## Read COMSOL mesh

#set mesh file name 
set fn {geometryFile}

#generate random seed from /dev/random
proc randInt {{ min max }} {{
    set randDev [open /dev/urandom rb]
    set random [read $randDev 8]
    binary scan $random H16 random
    set random [expr {{([scan $random %x] % (($max-$min) + 1) + $min)}}]
    close $randDev
    return $random
}}

#start reading the mesh

COMSOLReader R
    R filename $fn
    R read

set M [R mesh]

## create material set

{materialsDefinition}

# Create source
 PencilBeam P1
    P1 position "0.0001 0.0001 0"
    P1 direction "0 0 -1"

# Set up kernel

TetraTraceKernel k
    k packetCount {packetCount}
    k threadCount 8
    k randSeed [randInt 0 4294967295]
    puts [string cat "Random seed being used: " [k randSeed]]
    k source P1
    k geometry $M
    k materials MS

# Run and wait until completes
    k runSync

# Get results OutputDataCollection
set ODC [k results]

set results [$ODC getByName "PacketTraces"]

#create a 11x11 grid of detectors that are 1mm^2 each and centered at integer locations
for {{set x -5}} {{ $x <= 5 }} {{ incr x }} {{
        for {{set y -5}} {{ $y <= 5 }} {{ incr y}} {{
                file mkdir [string cat "results/csv_baseline_angles_x_" $x "_y_" $y]
                file mkdir [string cat "results/csv_pulsatile_angles_x_" $x "_y_" $y]
                file mkdir [string cat "results/csv_baseline_fluence_x_" $x "_y_" $y]
                file mkdir [string cat "results/csv_pulsatile_fluence_x_" $x "_y_" $y]

                filterTracesExterior filterBaseline
                        filterBaseline source $results
                        filterBaseline defineBoundary z 0 [expr $x - .5] [expr $y - .5] [expr $x + .5] [expr $y + .5]
                        filterBaseline update

                RegionFilter filterPulsatile
                        filterPulsatile source [filterBaseline results]
                        filterPulsatile kernel k
                        filterPulsatile region 5
                        filterPulsatile update

                filterTracesExterior filterTotalPulsatile
                        filterTotalPulsatile source [filterPulsatile results]
                        filterTotalPulsatile defineBoundary z 0 [expr $x - .5] [expr $y - .5] [expr $x + .5] [expr $y + .5]
                        filterTotalPulsatile update

                set baselineFile [string cat "results/csv_baseline_angles_x_" $x "_y_" $y "/sim_" [lindex $argv 0] ".csv" ]
                set pulsatileFile [string cat "results/csv_pulsatile_angles_x_" $x "_y_" $y "/sim_" [lindex $argv 0] ".csv" ]

                set baselineFluenceFile [open [string cat "results/csv_baseline_fluence_x_" $x "_y_" $y "/sim_" [lindex $argv 0] ".csv"] w]
                set pulsatileFluenceFile [open [string cat "results/csv_pulsatile_fluence_x_" $x "_y_" $y "/sim_" [lindex $argv 0] ".csv"] w]

                puts $baselineFluenceFile [filterBaseline total]
                puts $pulsatileFluenceFile [filterTotalPulsatile total]

                close $pulsatileFluenceFile
                close $baselineFluenceFile

                ConvertTracesToAngles anglesBaseline
                        anglesBaseline data [filterBaseline results]
                        anglesBaseline kernel k
                        anglesBaseline normal 0 0 1
                        anglesBaseline origin $x $y 0
                        anglesBaseline divisions 30
                        anglesBaseline update
                        anglesBaseline csvExport $baselineFile

                ConvertTracesToAngles anglesPulsatile
                        anglesPulsatile data [filterPulsatile results]
                        anglesPulsatile kernel k
                        anglesPulsatile normal 0 0 1
                        anglesPulsatile origin $x $y 0
                        anglesPulsatile divisions 30
                        anglesPulsatile update
                        anglesPulsatile csvExport $pulsatileFile
        }}
}}
