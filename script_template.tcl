package require FullMonte

#get the location of the script file and cd to
#its parent directory
set script_path [ file dirname [ file normalize [ info script ] ] ]
cd $script_path

## Read COMSOL mesh

#set mesh file name 
set fn {geometryFile}

#set lambda "1600"

#puts "reading mesh"

COMSOLReader R
    R filename $fn
    R read

set M [R mesh]

#puts [R tetRegionsReadOK]


## create material set

{materialsDefinition}

# Create source
 PencilBeam P1
    P1 position "0.001 0.001 0.00"
    P1 direction "0 0 -1"

# Set up kernel

TetraSVKernel k
    k packetCount {packetCount}
    k threadCount 8
    k source P1
    k geometry $M
    k materials MS

# Run and wait until completes
    #k runSync
    k runSync


# Get results OutputDataCollection
set ODC [k results]



#puts "EnergyToFluence conversion"
# Convert energy absorbed per volume element to volume average fluence
EnergyToFluence EF
    EF inputFluence
    #EF geometry $M
    #EF materials MS
    EF data [$ODC getByName "SurfaceExitEnergy"]
    EF outputFluence
    EF update

EnergyToFluence EV
    EV inputFluence
    #EV geometry $M
    #EV materials MS
    EV data [$ODC getByName "VolumeEnergy"]
    EV outputFluence
    EV update

#create the directories for the results -- repeated command, but it's indempotent
file mkdir "results/vtk_volume"
file mkdir "results/vtk_surface"

#puts "writing VTKMeshWriter"
# Write the mesh with fluence appended
VTKMeshWriter W
    W filename [string cat "results/vtk_volume/sim_" [lindex $argv 0] ".vtk"]
    W addData "Fluence" [EV result]
    W mesh $M
    W write


#puts "writing VTKSurfaceWriter"
#Write the surface fluence values
VTKSurfaceWriter SW
        SW filename [string cat "results/vtk_surface/sim_" [lindex $argv 0] ".vtk"]
        SW addData "surfaceFluence" [EF result]
        SW mesh $M
        SW write


#puts "writing TextFileMatrixWriter"
## Write the fluence values only to a text file
#TextFileMatrixWriter TW
#    TW filename [string cat "/results/" $lambda "test21.phi_v.txt"]
#    TW source [EF result]
#    TW write