import numpy as np
import csv
import os
import pandas as pd
from multiprocessing import Pool


def pulsatileAnglesParser(resultDirectory):
    partialDataframeRows = []
    #example directory name is csv_pulsatile_angles_x_-1_y_2
    for folder in os.listdir(resultDirectory):
        folderLabels = folder.split('_')
        if folderLabels[2] != 'angles':
            continue
        if folderLabels[1] == 'pulsatile':
            isPulsatile = True
        else:
            isPulsatile = False
        xPos = int(folderLabels[4])
        yPos = int(folderLabels[6])
        
        #combine the sim csvs into a mean and standard deviation
        results = []
        curDirectory = os.path.join(resultDirectory, folder)
        for file in os.listdir(curDirectory):
            absoluteFilePath = os.path.join(curDirectory, file)
            with open(absoluteFilePath) as fileHandle:
                simResult = []
                simCsvReader = csv.reader(fileHandle)
                for row in simCsvReader:
                    simResult.append(row)
                results.append(simResult)
        results = np.array(results, dtype = float)
        mean = np.sum(results, axis = 0)
        standardDeviation = np.std(results, axis = 0)

        partialDataframeRow = [isPulsatile, xPos, yPos, mean, standardDeviation]
        partialDataframeRows.append(partialDataframeRow)
    
    return partialDataframeRows

def pulsatileFluenceParser(resultDirectory):
    partialDataframeRows = []
    #example directory name is csv_pulsatile_fluence_x_-1_y_2
    for folder in os.listdir(resultDirectory):
        folderLabels = folder.split('_')
        if folderLabels[2] != 'fluence':
            continue
        if folderLabels[1] == 'pulsatile':
            isPulsatile = True
        else:
            isPulsatile = False
        xPos = int(folderLabels[4])
        yPos = int(folderLabels[6])
        
        #combine the sim csvs into a mean and standard deviation
        results = []
        curDirectory = os.path.join(resultDirectory, folder)
        for file in os.listdir(curDirectory):
            absoluteFilePath = os.path.join(curDirectory, file)
            with open(absoluteFilePath) as fileHandle:
                simResult = float(fileHandle.read().strip())
                results.append(simResult)
        results = np.array(results, dtype = float)
        mean = np.sum(results, axis = 0)
        standardDeviation = np.std(results, axis = 0)

        partialDataframeRow = [isPulsatile, xPos, yPos, mean, standardDeviation]
        partialDataframeRows.append(partialDataframeRow)
    
    return partialDataframeRows

def commit_results(simParserFunction, directory, traits): #function for parallizing
    leafSimResults = simParserFunction(os.path.join(directory, 'results'))
    numpyTraits = np.reshape(traits,(1,len(traits)))
    extendedTraits = np.repeat(numpyTraits, len(leafSimResults), axis=0)
    numpyResults = np.array(leafSimResults, dtype = object)
    dataframeReadyResults = np.concatenate((extendedTraits, numpyResults), axis=1)
    return dataframeReadyResults


class ResultParser:
    def __init__(self, directory):
        self.results = None
        self.labels = None
        self.rootDirectory = directory
        self.simParserFunction = None
        self.jobs = []
        
    def get_dataframe(self):
        if self.results is None: #ensure data is parsed
            self.parse_data()
        dataframes = []
        for result in self.results:
            dataframes.append(pd.DataFrame(data = result, columns = self.labels))
        return pd.concat(dataframes, ignore_index = True)
        
    def get_labels(self):
        directory = self.rootDirectory
        self.labels = []
        files = os.listdir(directory)
        while not 'results' in files:
            folder = files[0]
            parsedName = folder.split("_")
            traitName = parsedName[0]
            self.labels.append(traitName)
            directory = os.path.join(directory, folder)
            files = os.listdir(directory)
            
    def parse_data(self):
        if self.labels is None: #ensure labels are generated
            self.get_labels()
        if self.simParserFunction is None:
            print("Parser type is not selected. Please call function to select parser appropriate for data. Aborting")
            exit(1)
        self.parse_directory(self.rootDirectory, [])
        with Pool() as pool:
            self.results = pool.starmap(commit_results, self.jobs)
        #for item in self.jobs:
            #self.results = []
            #self.results.append(commit_results(*item))
        
    def parse_directory(self, directory, traits):
        files = os.listdir(directory)
        if 'results' in files: #this is a leaf node
            self.jobs.append((self.simParserFunction, directory, traits))
            
        else: #this is a kubeia tree span folder
            for spanFolder in files:
                parsedName = spanFolder.split("_")
                traitName = parsedName[0]
                traitValue = "_".join(parsedName[1:])
                newTraits = traits.copy()
                newTraits.append(traitValue)
                self.parse_directory(os.path.join(directory, spanFolder), newTraits)
                
    def select_pulsatile_angle_parser(self):
        if self.labels is None: #ensure labels are generated
            self.get_labels()
        #add the labels that are specific to the results and not the different simulation parameters
        self.labels += ['isPulsatile','detectorXPos','detectorYPos','anglesMean','anglesStd']
        self.simParserFunction = pulsatileAnglesParser
        
    def select_pulsatile_fluence_parser(self):
        if self.labels is None: #ensure labels are generated
            self.get_labels()
        #add the labels that are specific to the results and not the different simulation parameters
        self.labels += ['isPulsatile','detectorXPos','detectorYPos','fluenceMean','fluenceStd']
        self.simParserFunction = pulsatileFluenceParser

    def store_results(self, filename):
        df = self.get_dataframe()
        df.to_pickle(filename)
