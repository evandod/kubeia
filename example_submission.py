import kubeia

submission = kubeia.root('/home/ec2-user/shared_files')

submission.set_template('runCheek.tcl')

submission.set_submission_name('example_submission6')

submission.set_packets_per_simulation(2000000)

submission.set_simulation_divisions(3)

submission.add_span('opticalFile',['bashkatov.csv']) #required span name
submission.add_span('wavelength',[1000,1100,1200,1300,1400,1500,1600]) #required span name
submission.add_span('geometry',['tetraCheek.mphtxt']) #required span name
submission.add_span('regions',[['fat','skin','fat']] )#required span name

submission.push_submission()
