import stat
import boto3
import itertools
import os
import csv
import shutil

class root:
    def __init__(self, efsLocalMountPoint):
        self._initialize(efsLocalMountPoint);

    def _initialize(self, efsLocalMountPoint): #initializes AWS boto3 resources
        self.efs = boto3.client('efs');
        self.batch = boto3.client('batch');
        self.efsLocation = efsLocalMountPoint;
        self.spans = JobSpan()
        self.materialProperties = {} #nested dictionary, access desired material object by [wavelength : int][material_name : str]
        #self.tclTemplate
        #self.numSimDivisions
        #self.packetsPerSim
        #self.submissionName
        #self.geometryPath


    def set_template(self, templatePath):
        with open(templatePath, 'r') as file:
            self.tclTemplate = file.read()

    def set_simulation_divisions(self, numDivisions):
        self.numSimDivisions = int(numDivisions)

    def set_packets_per_simulation(self, numPackets):
        self.packetsPerSim = str(numPackets)

    def set_submission_name(self, name):
        self.submissionName = str(name)

    def set_geometry_path(self, filePath):
        self.geometryPath = str(filePath)

    def add_span(self, name, values):
        values.insert(0,name)
        self.spans.add_span(values)

    def push_submission(self):
        jobs,names = self.spans.list_jobs()
        significantNames = [span[0] for span in self.spans.get_spans() if len(span) > 2]
        for job in jobs:
            opticalFile = job[names.index('opticalFile')]
            self.parse_optical_properties_csv(opticalFile)
            parsedTemplate = self.parse_template(job, names)
            directories = list(name + "_" + str(job[names.index(name)]) for name in significantNames)
            pathToLeaf = os.path.join(self.efsLocation, "kubeia", self.submissionName, *directories)
            pathToLeafEfs = os.path.join("/kubeia", self.submissionName, *directories)
            pathToTcl = os.path.join(pathToLeaf, 'simulation.tcl')
            pathToTclEfs = os.path.join(pathToLeafEfs, 'simulation.tcl')
            os.makedirs(pathToLeaf)
            with open(pathToTcl, 'w') as templateWriter:
                templateWriter.write(parsedTemplate)
            geometryFileName = job[names.index('geometry')]
            shutil.copy2(geometryFileName, os.path.join(pathToLeaf, os.path.basename(geometryFileName)) )
            os.chmod(os.path.basename(geometryFileName),stat.S_IREAD | stat.S_IWRITE | stat.S_IROTH | stat.S_IRGRP)
            self.submit_job(pathToTclEfs, self.numSimDivisions)



    def parse_template(self, job, names):
        values = dict(zip(names,job))
        values['packetCount'] = self.packetsPerSim
        wavelength = int(values['wavelength'])
        geometryPath = os.path.basename(values['geometry'])
        values['geometryFile'] = '"' + geometryPath + '"'
        regions = values['regions']

        availableMaterials = self.materialProperties[wavelength]
        materialSet = MaterialSet()
        for region in regions:
            materialToAdd = availableMaterials[region]
            materialSet.add_material(materialToAdd)

        values['materialsDefinition'] = str(materialSet)



        parsedTemplate = self.tclTemplate.format_map(values)

        return parsedTemplate

    def parse_optical_properties_csv(self, csvPath):
        """
        parses a csv file containing optical properties for a material set generates the class objects

        format for the CSV file must be as follows:
        1. first row is the header containing names for the columns
        2. each column name must be the name of the material followed by an underscore followed by the property the column is describing
            or 'wavelength'
        3. The names of the properties that can be defined are: mus (µ_s), mua (µ_a), g (anisotropy), n (refractive index)
        """

        with open(csvPath,'r') as csvFile:
            reader = csv.reader(csvFile)
            fieldLabels = next(reader)
            fieldLabels = [label.lower() for label in fieldLabels]
            wavelengthIdx = fieldLabels.index('wavelength')
            properties = {}
            possibleParameters = {'mus','mua','g','n'}
            for row in reader:

                #break into seperate function of parse_optical_row
                materials = {}
                wavelength = int(row[wavelengthIdx])
                for columnIdx in range(len(row)):
                    if(columnIdx == wavelengthIdx):
                        continue
                    material, parameter = (label.lower() for label in fieldLabels[columnIdx].split("_") )
                    if parameter == 'reducedmus':
                        continue
                    elif parameter not in possibleParameters:
                        continue
                    value = float(row[columnIdx])
                    if material not in materials:
                        materials[material] = {}
                    materials[material][parameter] = value

                materialObjects = {}
                for material in materials.keys():
                    try:
                        mua = materials[material]['mua']
                        mus = materials[material]['mus']
                        g   = materials[material]['g']
                        n   = materials[material]['n']
                    except:
                        raise KeyError(f'material {material} is only partially defined in CSV file')
                    materialObject = Material(material, mua, mus, g, n)
                    materialObjects[material] = materialObject
                properties[wavelength] = materialObjects

            self.materialProperties = properties


    def submit_job(self, tcl_path, jobSize=1, vCPU = 16):
        """
        submits a job to AWS batch

        must have
        """
        print(tcl_path[1:].replace('/','_').replace(' ','-').replace('.','-').replace('simulation.tcl',''))
        self.batch.submit_job(
            jobName = tcl_path[1:].replace('/','_').replace(' ','-').replace('.','-').replace('simulation.tcl',''),
            jobQueue = 'fullmonteptp',
            jobDefinition = 'fullmonteptp',
            arrayProperties={
                'size': jobSize
            },
            containerOverrides={
                        "resourceRequirements": [
                           {
                              "type": "MEMORY",
                              "value": str(7900*vCPU)
                           },
                           {
                              "type": "VCPU",
                              "value": str(vCPU)
                           }
                        ],
                'command': ['/bin/sh', '-c', "cd " + os.path.dirname(tcl_path).replace(' ','\ ') + "; tclmonte.sh " + os.path.basename(tcl_path) +  " $AWS_BATCH_JOB_ARRAY_INDEX"] #must have an EFS mount 
            },
            retryStrategy = {
                'attempts': 2
            },
            timeout = {
                'attemptDurationSeconds': 3600
            }
        )

    def create_span(self):
        pass

    #def parse_template(self, spanNames, spanValues, templateLocation):
    #    """
    #    parses a tcl template
    #
    #    Special replacement points for the parse template are: {materialSet} which depends are wavelength, and nothing else so far.
    #    
    #    Other replacement points are substituted with values given in a span
    #    """
    #
    #    with open(templateLocation,'r') as templateHandler:
    #        template = templateHandler.read()





#class TemplateCreator:??


class MaterialSet:
    def __init__(self):
        self.materials = []
        self.airIndex = 1

    def __str__(self):
        output = "MaterialSet MS\n\n";
        air = Material('air', 0, 0, 1, self.airIndex)
        output += str(air) + "\n";
        definedMaterials = [];
        for material in self.materials:
            if material.getName() not in definedMaterials:
                output += str(material) + "\n";
                definedMaterials.append(material.getName())
        output += "MS exterior air\n"
        for materialIdx in range(0,len(self.materials)):
            output += "MS append " + str(self.materials[materialIdx].getName()) + "\n";
        return output

    def add_material(self, material):
        self.materials.append(material)



class Material:
    def __init__(self, name, µA, µS, g, n):
        self.name = name #name for the material
        self.muA = µA; #coefficient of absorption
        self.muS = µS; #coefficient of scattering
        self.g = g; #anisotropy
        self.n = n; #index of refraction

    def __str__(self):
        output = "Material " + self.name + "\n\t" + self.name + " absorptionCoeff\t" + str(self.muA) + "\n\t" + self.name + " scatteringCoeff\t" + str(self.muS) + \
            "\n\t" + self.name + " anisotropy\t" + str(self.g) + "\n\t" + self.name + " refractiveIndex\t" + str(self.n) + "\n"
        return output

    def getName(self):
        return self.name

class JobSpan:
    def __init__(self):
        self.spans = []

    def add_span(self, span):
        self.spans.append(span)
        return

    def get_spans(self):
        return iter(self.spans)


    def list_jobs(self): #creates a cartesian product of all the spans
        names = [span[0] for span in self.spans];
        inputs = [span[1:] for span in self.spans]
        values = list(itertools.product(*inputs))
        return values,names

    def make_span_tree(self, mount_point):
        self.span_helper(mount_point, 0)

    def span_helper(self, file_path, index):
        name = self.spans[index][0]
        for spanIdx in range(1,len(self.spans[index])):
            spanLabel = name + "_" + str(self.spans[index][spanIdx]);
            newFilePath = os.path.join(file_path, spanLabel)
            os.makedirs(newFilePath)
            if index < len(self.spans):
                self.span_helper(newFilePath, index + 1)

