#!/usr/bin/env python3
import argparse
import csv

#Adjusts the hematocrit for blood in a table of optical parameters
#this assumes the optical constants are given in units of 1/mm
#The method for this were taken from bosschaart 2014 

h2oMua = {400: 0.00058, 500: 0.00025, 600: 0.0023, 700: 0.0023, 800: 0.02, 900: 0.068, 1000: 0.36, 1100: 0.17, 1200: 1.04, 1300: 1.11, 1400: 12.390, 1500: 17.59, 1600: 6.72} #in units of 1/cm
#from https://omlc.org/spectra/water/data/hale73.txt


#from bosschaart 2014 eq. 2
plasmaWaterFraction = 0.9

RbcWaterFraction = 0.66

def blood_water_fract(hematocritPercentage):
    return (1-hematocritPercentage/100)*plasmaWaterFraction + hematocritPercentage/100 * RbcWaterFraction

#from bosschaart 2014 eq. 1 & 2
def adjust_absorption( givenAbsorption, givenHctFract, desiredHctFract, h2oMua):
    givenHct = givenHctFract*100
    desiredHct = desiredHctFract*100
    return desiredHct/givenHct * (givenAbsorption - (h2oMua/10) * blood_water_fract(givenHct)) + (h2oMua/10) * blood_water_fract(desiredHct) # /10 factor is converting from 1/cm to 1/mm

#from bosschaart 2014 eq. 17
def scalingMiePY(hct):
    return (1-hct) * (0.98 - hct)

#from bosschaart 2014 eq. 13
def adjust_scattering(givenScattering, givenHct, desiredHct):
    return (scalingMiePY(desiredHct) / scalingMiePY(givenHct)) * (desiredHct / givenHct) * givenScattering

def convertOpticalParameters(inputFile, outputFile, givenHct, desiredHct):
    #with open(fileName, 'r') as fileRead, open(outputFile,'w') as fileWrite:
    reader = csv.DictReader(inputFile)
    writer = csv.DictWriter(outputFile, fieldnames=reader.fieldnames)
    writer.writeheader()
    for row in reader:
        newMua = adjust_absorption(float(row['blood_mua']), givenHct, desiredHct, h2oMua[int(row['wavelength'])])
        newMus = adjust_scattering(float(row['blood_mus']), givenHct, desiredHct)
        newRow = row.copy()
        newRow['blood_mua'] = newMua
        newRow['blood_mus'] = newMus
        writer.writerow(newRow)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Changes the hematocrit percentage in a .csv file of optical parameters by transforming the blood optical parameters')
    parser.add_argument('inputFile',type=argparse.FileType('r'),help='the .csv file of optical parameters to transform')
    parser.add_argument('outputFile', type=argparse.FileType('w'), help='the file to write out the transformed optical parameters')
    parser.add_argument('givenHct', type=float, help='the hematocrit ratio of the inputFile')
    parser.add_argument('desiredHct', type=float, help='the hematocrite ratio of the outputFile')
    args = parser.parse_args()
    if not ( 0 <= args.givenHct <= 1 and 0 <= args.desiredHct <= 1):
        print("WARNING: Hct value(s) below 0% or above 100% -- Are you sure this is what you want?")
    convertOpticalParameters(args.inputFile,args.outputFile,args.givenHct,args.desiredHct)
